# Ansible role: Docker

Install Docker, standalone or in swarm mode.

## Licence

BSD 3-Clause, see the [LICENSE](./LICENSE) file.

## Role Variables

* `docker_partition_size`: size passed to LVM, used for `/var/lib/docker/` (at least 100G is suggested)
* `docker_package`: a Docker distribution, ie: `docker-ce`, or `docker-ee`, or `docker-ce=18.03*`
* `docker_swarm_manager`: configure the corresponding ansible `inventory_hostname` to be a swarm manager
* `docker_swarm_domain`: base domain to access the swarm cluster; see DNS Configuration
